﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    // Variables
    public GameObject[] animalPrefabs = new GameObject[3];

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        int index = Random.Range(0, animalPrefabs.length);
        Vector3 spawnPos = new Vector3(Random.Range(-5f, 5f),0,20);
        if (Input.GetKeyDown(KeyCode.S))
        {
            Instantiate(animalPrefabs[index], spawnPos, animalPrefabs[index].transform.rotation);
        }
        
    }
}
